﻿using Microsoft.EntityFrameworkCore;
using TaskApi.Context;
using TaskApi.ITaskRepository;
using TaskApi.Models;

namespace TaskApi.TaskRepository
{
    public class SubTaskRepo:ISubTask
    {
        TaskApiDbContext _context;
        public SubTaskRepo(TaskApiDbContext context)
        {
            _context = context;
        }
        public List<SubTask> GetSubTasksByTask(int id)
        {
            return _context.SubTasks.Where(x => x.TaskId==id).ToList();

        }

        public void AddSubTask(SubTask sbtsk) { 
        _context.SubTasks.Add(sbtsk);
            _context.SaveChanges();
        }
    }
}
