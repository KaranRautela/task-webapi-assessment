﻿using TaskApi.Context;
using TaskApi.ITaskRepository;
using TaskApi.Models;

namespace TaskApi.TaskRepository
{
    public class TaskRepo:ITask
    {
        TaskApiDbContext _context;
        public TaskRepo(TaskApiDbContext context)
        {
            _context = context;
        }
        public void AddTask(Task1 task)
        {
            _context.Tasks.Add(task);
            _context.SaveChanges();
        }

        public void DeleteTask(int id)
        {
            Task1 task = _context.Tasks.FirstOrDefault(x => x.Id == id);
            if (task != null)
            {
                _context.Tasks.Remove(task);
                _context.SaveChanges();
            }
        }

        public void EditTask(int id, Task1 task)
        {
            Task1 task1 = _context.Tasks.FirstOrDefault(x => x.Id == id);
            if (task1 != null)
            {
                task1.TaskName=task.TaskName;
                task1.TaskDescription = task.TaskDescription;
                task1.CreatedBy = task.CreatedBy;
                task1.CreatedOn = task.CreatedOn;
                _context.SaveChanges();
            }
        }
        public Task1 GetTaskById(int id)
        {
            Task1 task = _context.Tasks.FirstOrDefault(x => x.Id == id);
            return task;
        }
        public List<Task1> GetTasks()
        {
            return _context.Tasks.ToList();

        }

        public List<SubTask> GetSubTasks(int id)
        {
            return _context.SubTasks.Where(x => x.TaskId == id).ToList();
        }
    }
}
